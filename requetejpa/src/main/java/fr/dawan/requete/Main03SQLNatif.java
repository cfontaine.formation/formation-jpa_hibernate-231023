package fr.dawan.requete;

import java.util.List;

import fr.dawan.requete.entities.Article;
import fr.dawan.requete.entities.Marque;
import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.Persistence;

public class Main03SQLNatif {

    @SuppressWarnings("unchecked")
    public static void main(String[] args) {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("requetejpa");
        EntityManager em = emf.createEntityManager();

        // Requête native SQL
        List<Article> lst = em.createNativeQuery("SELECT * FROM articles a WHERE a.prix<:prix LIMIT 2", Article.class)
                .setParameter("prix", 100.0)
                .getResultList();
        for (Article a : lst) {
            System.out.println(a);
        }

        // Requête native nommée
        List<Marque> lstMarque = em.createNamedQuery("Marque.findbynom", Marque.class)
                .setParameter("nom", "Marque A")
                .getResultList();
        System.out.println(lstMarque);

        em.close();
        emf.close();
    }

}
