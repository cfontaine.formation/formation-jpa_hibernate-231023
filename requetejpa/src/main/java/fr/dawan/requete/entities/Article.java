package fr.dawan.requete.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.NamedQueries;
import jakarta.persistence.NamedQuery;
import jakarta.persistence.NamedStoredProcedureQuery;
import jakarta.persistence.ParameterMode;
import jakarta.persistence.StoredProcedureParameter;
import jakarta.persistence.Table;
import jakarta.persistence.Version;

@Entity
@Table(name = "articles")
@NamedQueries(value= {
        @NamedQuery(name="Article.prixless",query="SELECT a FROM Article a WHERE a.prix<?1"),
        @NamedQuery(name="Article.descriptionPrefix",query = "SELECT a FROM Article a WHERE a.description LIKE CONCAT(:motif,'%')")
})
@NamedStoredProcedureQuery(name = "addition", procedureName = "calcul", parameters = {
        @StoredProcedureParameter(mode = ParameterMode.IN, type = Integer.class, name = "x"),
        @StoredProcedureParameter(mode = ParameterMode.IN, type = Integer.class, name = "y"),
        @StoredProcedureParameter(mode = ParameterMode.OUT, type = Integer.class, name = "somme") })
public class Article implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Version
    private int version;

    @Column(nullable = false)
    private double prix;

    @Column(length = 100, nullable = false)
    private String description;

    @ManyToOne
    @JoinColumn(name = "id_marque")
    private Marque marque;

    @ManyToMany(mappedBy = "articles")
    private List<Fournisseur> fournisseurs = new ArrayList<>();

    public Article() {
    }

    public Article(double prix, String description) {
        this.prix = prix;
        this.description = description;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public double getPrix() {
        return prix;
    }

    public void setPrix(double prix) {
        this.prix = prix;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Marque getMarque() {
        return marque;
    }

    public void setMarque(Marque marque) {
        this.marque = marque;
    }

    public void addMarque(Marque marque) {
        this.marque = marque;
        marque.getArticles().add(this);
    }

    public List<Fournisseur> getFournisseurs() {
        return fournisseurs;
    }

    public void setFournisseurs(List<Fournisseur> fournisseurs) {
        this.fournisseurs = fournisseurs;
    }

    @Override
    public String toString() {
        return "Article [id=" + id + ", version=" + version + ", prix=" + prix + ", description=" + description + "]";
    }

}