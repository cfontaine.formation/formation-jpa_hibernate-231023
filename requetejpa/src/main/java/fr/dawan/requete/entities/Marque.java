package fr.dawan.requete.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.NamedAttributeNode;
import jakarta.persistence.NamedEntityGraph;
import jakarta.persistence.NamedNativeQueries;
import jakarta.persistence.NamedNativeQuery;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import jakarta.persistence.Version;

@Entity
@Table(name = "marques")
@NamedEntityGraph(
        name="marquegraph",
        attributeNodes = {
                @NamedAttributeNode("articles")
        }
        )
@NamedNativeQueries({
    @NamedNativeQuery(name = "Marque.findbynom",
            query= "SELECT * FROM marques WHERE nom= :nom",
            resultClass = Marque.class)
})
public class Marque implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Version
    private int version;

    @Column(length = 50, nullable = false)
    private String nom;

    @OneToMany(mappedBy = "marque", cascade = CascadeType.ALL, orphanRemoval = true) // ,fetch=FetchType.EAGER
    private List<Article> articles = new ArrayList<>();

    public Marque() {
    }

    public Marque(String nom) {
        this.nom = nom;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public List<Article> getArticles() {
        return articles;
    }

    public void setArticles(List<Article> articles) {
        this.articles = articles;
    }

    @Override
    public String toString() {
        return "Marque [id=" + id + ", nom=" + nom + "]";
    }

}