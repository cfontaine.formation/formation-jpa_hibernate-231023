package fr.dawan.requete;

import fr.dawan.requete.entities.Article;
import fr.dawan.requete.entities.Marque;
import jakarta.persistence.EntityGraph;
import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.Persistence;
import jakarta.persistence.TypedQuery;

public class Main02EntityGraph {

    public static void main(String[] args) {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("requetejpa");
        EntityManager em = emf.createEntityManager();
        
       // Marque ma=em.find(Marque.class, 1L);

        // Join fetch
        //        Marque ma=em.createQuery("SELECT m FROM Marque m JOIN FETCH m.articles WHERE m.id=1",Marque.class).getSingleResult();
        //        System.out.println(ma);
        // EntityGraph
        EntityGraph<?> graph=em.createEntityGraph("marquegraph");
        TypedQuery<Marque> tq=em.createQuery("SELECT m FROM Marque m WHERE m.id=1",Marque.class);
        tq.setHint("jakarta.persistence.loadgraph", graph);
        Marque ma=tq.getSingleResult();
        em.close();
        
        
        System.out.println(ma.getArticles().get(0));

        emf.close();
    }

}
