package fr.dawan.requete;

import java.util.List;

import fr.dawan.requete.entities.Article;
import fr.dawan.requete.entities.ArticleSimple;
import fr.dawan.requete.entities.Marque;
import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.EntityTransaction;
import jakarta.persistence.ParameterMode;
import jakarta.persistence.Persistence;
import jakarta.persistence.Query;
import jakarta.persistence.StoredProcedureQuery;
import jakarta.persistence.TypedQuery;

public class Main01JPQL {

    @SuppressWarnings("unchecked")
    public static void main(String[] args) {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("requetejpa");
        EntityManager em = emf.createEntityManager();
        EntityTransaction tx = em.getTransaction();

        // En SQL: SELECT * FROM articles
        // En JPQL: SELECT a FROM Article as a

        // createQuery -> permet de créer les requêtes JPQL depuis l’Entity Manager
        // getSingleResult -> pour récupérer un résultat unique, s'il n'y a pas de
        // résulat ou plusieurs => exception
        // getResultList() -> pour récupérer un résultat multiple (List)

        // Query et TypedQuery: représente la requête
        // Query
        Query query1 = em.createQuery("SELECT a FROM Article as a");
        List<Article> ar = (List<Article>) query1.getResultList();
        for (Article a : ar) {
            System.out.println(a);
        }

        // TypedQuery -> à privilégier par rapport à Query => Type Générique
        TypedQuery<Article> query2 = em.createQuery("SELECT a FROM Article as a", Article.class);
        ar = query2.getResultList();
        for (Article a : ar) {
            System.out.println(a);
        }

        //
        TypedQuery<String> query3 = em.createQuery("SELECT a.description FROM Article a", String.class);
        List<String> lstStr = query3.getResultList();
        for (String s : lstStr) {
            System.out.println(s);
        }

        Query query4 = em.createQuery("SELECT a.prix, a.description FROM Article a");
        List<Object[]> lstObj = query4.getResultList();
        for (Object[] tObj : lstObj) {
            System.out.println(tObj[0] + " " + tObj[1]);
        }

        // new
        TypedQuery<ArticleSimple> query5 = em.createQuery("SELECT new ArticleSimple(a.description,a.prix) FROM Article a", ArticleSimple.class);
        List<ArticleSimple> lstArs = query5.getResultList();
        for (ArticleSimple a : lstArs) {
            System.out.println(a);
        }

        // DISTINCT
        TypedQuery<String> query6 = em.createQuery("SELECT DISTINCT a.description FROM Article a", String.class);
        List<String> lstStr2 = query6.getResultList();
        for (String s : lstStr2) {
            System.out.println(s);
        }

        // WHERE
        TypedQuery<Article> query7 = em.createQuery("SELECT a FROM Article a WHERE a.prix<50.0", Article.class);
        List<Article> lstAr2 = query7.getResultList();
        for (Article a : lstAr2) {
            System.out.println(a);
        }

        // Paramètres de requête

        // position -> ?numreo_position
        TypedQuery<Article> query8 = em.createQuery("SELECT a FROM Article a WHERE a.prix<?1 AND a.prix>?2", Article.class);
        query8.setParameter(1, 100.0); // -> setParameter, pour définir la valeur des paramètres 
        query8.setParameter(2, 30.0);
        List<Article> lstAr3 = query8.getResultList();
        for (Article a : lstAr3) {
            System.out.println(a);
        }

        // paramètre nommés -> :nomParamètre
        TypedQuery<Article> query9 = em.createQuery("SELECT a FROM Article a WHERE a.prix<:prixMax AND a.prix>:prixMin",
                Article.class);
        query9.setParameter("prixMax", 100.0);
        query9.setParameter("prixMin", 30.0);
        List<Article> lstAr4 = query9.getResultList();
        for (Article a : lstAr4) {
            System.out.println(a);
        }

        // BETWEEN -> définir un interval
        TypedQuery<Article> query10 = em
                .createQuery("SELECT a FROM Article a WHERE a.prix BETWEEN :prixMin AND :prixMax", Article.class);
        query10.setParameter("prixMax", 100.0);
        query10.setParameter("prixMin", 30.0);
        List<Article> lstAr5 = query10.getResultList();
        for (Article a : lstAr5) {
            System.out.println(a);
        }

        // IN -> définir un ensemble de valeur
        TypedQuery<Article> query11 = em.createQuery("SELECT a FROM Article a WHERE a.prix IN(40.0,80.0,350.0)", Article.class);
        query11.getResultList().forEach(System.out::println); // (m) -> System.out.println(m)

        // LIKE -> recherche sur un modèle particulier
        // Jokers: % -> 0,1 ou n caractères, – _ -> 1 caractère
        // Ici, on recherche tous les articles dont la description commence par M et fait au moins 4 caractères
        TypedQuery<Article> query12 = em.createQuery("SELECT a FROM Article a WHERE a.description LIKE 'M____%'", Article.class);
        query12.getResultList().forEach(System.out::println);

        // ESCAPE -> pour utiliser % ou _ en temps que caractère pour qu'ils ne soient pas interprété comme un Joker
        TypedQuery<Article> query13 = em
                .createQuery("SELECT a FROM Article a WHERE a.description LIKE '%100@%%' ESCAPE '@'", Article.class);
        query13.getResultList().forEach(System.out::println);

        // IS NULL -> pour tester si un variable d'instance est null
        TypedQuery<Article> query14 = em.createQuery("SELECT a FROM Article a WHERE a.marque IS NULL", Article.class);
        query14.getResultList().forEach(System.out::println);

        // IS EMPTY -> permet de tester, si une colection est vide
        TypedQuery<Marque> query15 = em.createQuery("SELECT m FROM Marque m WHERE m.articles IS EMPTY", Marque.class);
        query15.getResultList().forEach(System.out::println);

        // Expression de chemin

        // Pour un @OneToOne, @ManyToONE
        // marque est une "simple" variable d'instense, on peut accéder au nom
        TypedQuery<Article> query16 = em.createQuery("SELECT a FROM Article a WHERE a.marque.nom ='Marque C'", Article.class);
        query16.getResultList().forEach(System.out::println);

        // @ManyToMany et @OneToMany -> ERREUR
        // ERREUR articles est une collection => il faut faire une jointure
//        TypedQuery<Marque> query17 = em.createQuery("SELECT m FROM Marque m WHERE m.articles.prix<100.0", Marque.class);
//        query17.getResultList().forEach(System.out::println);

        // Fonction d'agrégation
        // COUNT -> compte le nombre d’élément
        TypedQuery<Long> query18 = em.createQuery("SELECT COUNT(a) FROM Article a WHERE a.prix>100.0", Long.class);
        System.out.println(query18.getSingleResult());

        // AVG -> Moyenne
        double moyPrix = em.createQuery("SELECT AVG(a.prix) FROM Article a", Double.class).getSingleResult();
        System.out.println(moyPrix);

        // Fonction chaine de caractère
        // CONCAT -> concaténation de chaine de caractères
        TypedQuery<String> query19 = em.createQuery("SELECT CONCAT(a.description,':',LENGTH(a.description)) FROM Article a", String.class);
        query19.getResultList().forEach(System.out::println);

        // SUBSTRING
        TypedQuery<String> query20 = em.createQuery("SELECT UPPER(SUBSTRING(a.description,1,4)) FROM Article a",String.class);
        query20.getResultList().forEach(System.out::println);

        // LOCATE -> recherche d’une sous-chaîne et retourne la position
        TypedQuery<Integer> query21 = em.createQuery("SELECT LOCATE('Tv',a.description) FROM Article a", Integer.class);
        query21.getResultList().forEach(System.out::println);

        // SIZE ->
        TypedQuery<Integer> query22 = em.createQuery("SELECT SIZE(m.articles) FROM Marque m", Integer.class);
        query22.getResultList().forEach(System.out::println);

        // ORDER BY -> trier, par défaut le tri est croissant, DESC pour qu'il soit décroissant
        // Il peut y avoir plusieurs critères de tri
        TypedQuery<Article> query23 = em.createQuery("SELECT a FROM Article a ORDER BY a.description DESC,a.prix", Article.class);
        query23.getResultList().forEach(System.out::println);

        // GROUP BY -> pour grouper plusieurs résultats, On peut regrouper sur un alias ou sur attribut
        TypedQuery<String> query24 = em.createQuery(
                "SELECT CONCAT(a.marque.nom, ':',COUNT(a.marque)) FROM Article a GROUP BY a.marque HAVING COUNT(a.marque)>3", String.class);
        query24.getResultList().forEach(System.out::println);

        // JOIN -> Jointure Interne
        TypedQuery<String> query25 = em.createQuery(
                "SELECT CONCAT(m.nom, ' ',a.description,' ',a.prix)FROM Marque m JOIN m.articles a", String.class);
        query25.getResultList().forEach(System.out::println);
        em.close();

        Query query26 = em.createQuery("SELECT a,m FROM Marque m JOIN m.articles a");
        List<Object[]> lstObj2 = query26.getResultList();
        for (Object[] tObj : lstObj2) {
            System.out.println(tObj[0] + " " + tObj[1]);
        }
        
        TypedQuery<String> query27 = em.createQuery(
                "SELECT CONCAT(f.nom,' ',a.description, ' ',a.marque.nom) FROM Article a JOIN a.fournisseurs f WHERE f.nom='Fournisseur 1'",
                String.class);
        query27.getResultList().forEach(System.out::println);

        // LEFT JOIN -> jointure externe
        TypedQuery<String> query28 = em.createQuery(
                "SELECT CONCAT(m.nom, ' : ',count(a)) FROM Marque m LEFT JOIN m.articles a GROUP BY m", String.class);
        query28.getResultList().forEach(System.out::println);

        // Sous-requête
        TypedQuery<Marque> query29 = em.createQuery("SELECT m FROM Marque m LEFT JOIN m.articles a WHERE a IS NULL", Marque.class);
        query29.getResultList().forEach(System.out::println);
        em.close();
        
        TypedQuery<String> query30 = em.createQuery("SELECT CONCAT(ar.description,' ',ar.marque.nom) FROM Article ar WHERE (SELECT Count(a) FROM Article a WHERE ar.marque=a.marque)>3", String.class);
        query30.getResultList().forEach(System.out::println);

        // NamedQuery
        // Requètes nommées ->  amélioration des performances (pré compilées par le
        //                      fournisseur de persistance au démarrage de l’application)
        // createNamedQuery -> permet de les utiliser avec l'entity manager
        TypedQuery<Article> query31 = em.createNamedQuery("Article.prixless", Article.class);
        query31.setParameter(1, 50.0);
        query31.getResultList().forEach(System.out::println);

        TypedQuery<Article> query32 = em.createNamedQuery("Article.descriptionPrefix", Article.class);
        query32.setParameter("motif", "M%");
        query32.getResultList().forEach(System.out::println);

        // setMaxResults -> correspond à LIMIT en SQL
        TypedQuery<Article> query33 = em.createQuery("SELECT a FROM Article a ORDER BY a.prix DESC", Article.class);
        query33.setMaxResults(5);
        query33.getResultList().forEach(System.out::println);


        // UPDATE =>    pour mettre à jour à un ensemble d'entité
        //              Doit obligatoirement se trouver dans une transaction
        //              executeUpdate permet d'exécuter la requête dans l'entity manager
        try {
            tx.begin();
            Query queryUpdate = em.createQuery("UPDATE Article a SET a.prix=a.prix*1.1 WHERE a.prix>:prixmin");
            queryUpdate.setParameter("prixmin", 50.0);
            int nbEntiteUpdate = queryUpdate.executeUpdate();
            System.out.println(nbEntiteUpdate); // retourne le nombre d'entité mis à jour
            tx.commit();
        } catch (Exception e) {
            tx.rollback();
            e.printStackTrace();
        }
        
        // DELETE -> pour supprimer un ensemble d'entité
        //           Doit obligatoirement se trouver dans une transaction
        //           executeUpdate pour exécuter la requête
        try {
            tx.begin();
            Query queryUpdate = em.createQuery("DELETE FROM  Article a WHERE a.marque IS NULL");
            int nbEntiteUpdate = queryUpdate.executeUpdate();
            System.out.println(nbEntiteUpdate); // retourne le nombre d'entité supprimer
            tx.commit();
        } catch (Exception e) {
            tx.rollback();
            e.printStackTrace();
        }

        // Procédure stockée
        
        StoredProcedureQuery procedureQuery = em.createStoredProcedureQuery("calcul");
        procedureQuery.registerStoredProcedureParameter("x", Integer.class, ParameterMode.IN);
        procedureQuery.registerStoredProcedureParameter("y", Integer.class, ParameterMode.IN);
        procedureQuery.registerStoredProcedureParameter("somme", Integer.class, ParameterMode.OUT);

        procedureQuery.setParameter("x", 2);
        procedureQuery.setParameter("y", 1);
        procedureQuery.execute();

        int somme = (Integer) procedureQuery.getOutputParameterValue("somme");
        System.out.println(somme);

     // Procédure stockée nommée
        StoredProcedureQuery procedureQuery2 = em.createNamedStoredProcedureQuery("addition");
        procedureQuery2.setParameter("x", 2);
        procedureQuery2.setParameter("y", 1);
        procedureQuery2.execute();
        System.out.println((Integer) procedureQuery.getOutputParameterValue("somme"));
        em.close();
        emf.close();
    }

}
