package fr.dawan.exercicejpa.entities;

import java.io.Serializable;

import jakarta.persistence.EmbeddedId;
import jakarta.persistence.Entity;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.MapsId;

@Entity
public class PizzaCommande implements Serializable {

    private static final long serialVersionUID = 1L;

    @EmbeddedId
    private PizzaCommandePK id = new PizzaCommandePK();

    @ManyToOne
    @MapsId("pizzaId")
    private Pizza pizza;

    @ManyToOne
    @MapsId("commandeId")
    private Commande commande;

    private int quantite;

    public PizzaCommande() {
    }

    public PizzaCommande(Pizza pizza, Commande commande, int quantite) {
        this.pizza = pizza;
        this.commande = commande;
        this.quantite = quantite;
    }

    public PizzaCommandePK getId() {
        return id;
    }

    public void setId(PizzaCommandePK id) {
        this.id = id;
    }

    public Pizza getPizza() {
        return pizza;
    }

    public void setPizza(Pizza pizza) {
        this.pizza = pizza;
    }

    public Commande getCommande() {
        return commande;
    }

    public void setCommande(Commande commande) {
        this.commande = commande;
    }

    public int getQuantite() {
        return quantite;
    }

    public void setQuantite(int quantite) {
        this.quantite = quantite;
    }

}
