package fr.dawan.exercicejpa.entities;

import jakarta.persistence.Embeddable;

@Embeddable
public class PizzaCommandePK {

    private long pizzaId;

    private long commandeId;

    public PizzaCommandePK() {

    }

    public PizzaCommandePK(long pizzaId, long commandeId) {
        this.pizzaId = pizzaId;
        this.commandeId = commandeId;
    }

    public long getPizzaId() {
        return pizzaId;
    }

    public void setPizzaId(long pizzaId) {
        this.pizzaId = pizzaId;
    }

    public long getCommandeId() {
        return commandeId;
    }

    public void setCommandeId(long commandeId) {
        this.commandeId = commandeId;
    }

}
