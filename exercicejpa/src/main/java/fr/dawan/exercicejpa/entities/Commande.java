package fr.dawan.exercicejpa.entities;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import jakarta.persistence.Version;

@Entity
@Table(name = "commandes")
public class Commande implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Version
    private int version;

    @Column(name = "heure_commande", nullable = false)
    private LocalDateTime heureCommande;

    @Column(name = "heure_livraison", nullable = false)
    private LocalDateTime heureLivraison;

    @ManyToOne
    private Client client;

    @ManyToOne
    private Livreur livreur;

    @OneToMany(mappedBy = "commande")
    private List<PizzaCommande> pizzas = new ArrayList<>();

    public Commande() {
    }

    public Commande(LocalDateTime heureCommande, LocalDateTime heureLivraison) {
        this.heureCommande = heureCommande;
        this.heureLivraison = heureLivraison;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public LocalDateTime getHeureCommande() {
        return heureCommande;
    }

    public void setHeureCommande(LocalDateTime heureCommande) {
        this.heureCommande = heureCommande;
    }

    public LocalDateTime getHeureLivraison() {
        return heureLivraison;
    }

    public void setHeureLivraison(LocalDateTime heureLivraison) {
        this.heureLivraison = heureLivraison;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public Livreur getLivreur() {
        return livreur;
    }

    public void setLivreur(Livreur livreur) {
        this.livreur = livreur;
    }

    @Override
    public String toString() {
        return "Commande [id=" + id + ", heureCommande=" + heureCommande + ", heureLivraison=" + heureLivraison + "]";
    }

}
