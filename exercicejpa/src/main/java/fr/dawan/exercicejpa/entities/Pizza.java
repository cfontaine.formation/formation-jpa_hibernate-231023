package fr.dawan.exercicejpa.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Lob;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import jakarta.persistence.Version;

@Entity
@Table(name = "pizzas")
public class Pizza implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Version
    private int version;

    @Column(length = 60, nullable = false)
    private String nom;

    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    private TypeBase base;

    private double prix;

    @Lob
    @Column(length = 60000)
    private byte[] photo;

    @ManyToMany(mappedBy = "pizzas")
    private List<Ingredient> ingredients = new ArrayList<>();

    @OneToMany(mappedBy = "pizza")
    private List<PizzaCommande> commandes = new ArrayList<>();

    public Pizza() {
    }

    public Pizza(String nom, TypeBase base, double prix) {
        this.nom = nom;
        this.base = base;
        this.prix = prix;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public TypeBase getBase() {
        return base;
    }

    public void setBase(TypeBase base) {
        this.base = base;
    }

    public double getPrix() {
        return prix;
    }

    public void setPrix(double prix) {
        this.prix = prix;
    }

    public byte[] getPhoto() {
        return photo;
    }

    public void setPhoto(byte[] photo) {
        this.photo = photo;
    }

    public List<Ingredient> getIngredients() {
        return ingredients;
    }

    public void setIngredients(List<Ingredient> ingredients) {
        this.ingredients = ingredients;
    }

    @Override
    public String toString() {
        return "Pizza [id=" + id + ", nom=" + nom + ", base=" + base + ", prix=" + prix + ", photo="
                + Arrays.toString(photo) + "]";
    }

}
