package fr.dawan.exercicejpa;

import fr.dawan.exercicejpa.entities.Ingredient;
import fr.dawan.exercicejpa.entities.Pizza;
import fr.dawan.exercicejpa.entities.TypeBase;
import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.EntityTransaction;
import jakarta.persistence.Persistence;

public class MainPizzeria {

    public static void main(String[] args) {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("exercicejpa");
        EntityManager em=emf.createEntityManager();
        EntityTransaction tx=em.getTransaction();
        
        Ingredient mozza=new Ingredient("Mozzarella");
        Pizza margherita=new Pizza("Margherita", TypeBase.ROUGE,11.0);
        Pizza pizzaSaumon=new Pizza("Pizza  au Saumon", TypeBase.BLANCHE,16.5);

        try {
            tx.begin();
            em.persist(mozza);
            em.persist(margherita);
            em.persist(pizzaSaumon);
            
            margherita.setPrix(margherita.getPrix()*1.1);
            pizzaSaumon.setPrix(pizzaSaumon.getPrix()*1.1);
            
            em.remove(mozza);
            tx.commit();
        } catch (Exception e) {
            e.printStackTrace();
            tx.rollback();
        }
        
        em.close();
        emf.close();
    }

}
