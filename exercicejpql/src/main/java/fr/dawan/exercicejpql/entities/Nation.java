package fr.dawan.exercicejpql.entities;

import java.util.ArrayList;
import java.util.List;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;

@Entity
@Table(name = "nations")
public class Nation extends AbstractEntity {

    private static final long serialVersionUID = 1L;

    @Column(nullable = false, length = 100)
    private String nom;

    @OneToMany(mappedBy = "nation")
    private List<Auteur> auteurs = new ArrayList<>();

    public Nation() {

    }

    public Nation(String nom) {
        this.nom = nom;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public List<Auteur> getAuteurs() {
        return auteurs;
    }

    public void setAuteurs(List<Auteur> auteurs) {
        this.auteurs = auteurs;
    }

    @Override
    public String toString() {
        return "Nation [" + super.toString() + ", nom =" + nom + "]";
    }
}
