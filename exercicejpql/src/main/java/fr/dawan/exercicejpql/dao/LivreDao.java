package fr.dawan.exercicejpql.dao;

import java.util.ArrayList;
import java.util.List;

import fr.dawan.exercicejpql.entities.Auteur;
import fr.dawan.exercicejpql.entities.Categorie;
import fr.dawan.exercicejpql.entities.Livre;
import jakarta.persistence.EntityManager;
import jakarta.persistence.TypedQuery;

public class LivreDao extends AbstractDao<Livre> {

    private static final long serialVersionUID = 1L;

    public LivreDao(EntityManager em) {
        super(em, Livre.class);
    }

    // Méthode qui retourne tous les livres sortie l'année passé en paramètre
    public List<Livre> findByAnnee(int anneeSortie) {
        TypedQuery<Livre> query = em.createQuery("SELECT l FROM Livre l WHERE l.anneeSortie= :annee", Livre.class);
        query.setParameter("annee", anneeSortie);
        return query.getResultList();
    }

    // Méthode qui retourne tous les livres sortie entre les 2 années passés en paramètre
    public List<Livre> findByIntervalAnnee(int anneeMin, int anneeMax) {
        TypedQuery<Livre> query = em.createQuery("SELECT l FROM Livre l WHERE l.anneeSortie BETWEEN :min AND :max", Livre.class);
        query.setParameter("min", anneeMin);
        query.setParameter("max", anneeMax);
        return query.getResultList();
    }

    // Méthode qui retourne tous les livres qui commencent par le préfixe passé en paramètre (en utilisant CONCAT)
    public List<Livre> findStartWith(String prefix) {
        TypedQuery<Livre> query = em.createQuery("SELECT l FROM Livre l WHERE l.titre LIKE CONCAT(:pre,'%')", Livre.class);
        query.setParameter("pre", prefix + "%");
        return query.getResultList();
    }

    // Méthode qui retourne tous les livres sortie pour les années passées en paramètre
    public List<Livre> findByAnnees(int... anneesSortie) {
        TypedQuery<Livre> query = null;
        if (anneesSortie.length > 0) {
            StringBuilder req = new StringBuilder("SELECT l FROM Livre l WHERE l.anneeSortie IN ( ");
            req.append(anneesSortie[0]);
            for (int i = 1; i < anneesSortie.length; i++) {
                req.append(",");
                req.append(anneesSortie[i]);
            }
            req.append(")");
            System.out.println(req.toString());
            query = em.createQuery(req.toString(), Livre.class);
            return query.getResultList();
        }
        return new ArrayList<>();

    }
    
    // Méthode qui retourne le nombre de livre sortie l'année passé en paramètre
    public long countByAnneeSortie(int anneeSortie) {
        TypedQuery<Long> query = em.createQuery("SELECT COUNT(l) FROM Livre l WHERE l.anneeSortie=:annee", Long.class);
        query.setParameter("annee", anneeSortie);
        return query.getSingleResult();
    }

    // Méthode qui retourne tous les livres pour une catégorie trié par année de sortie décroissant
    public List<Livre> findByCategorie(Categorie categorie) {
        TypedQuery<Livre> query = em.createQuery("SELECT l FROM Livre l WHERE l.categorie= :categorie ORDER BY l.anneeSortie DESC", Livre.class);
        query.setParameter("categorie", categorie);
        return query.getResultList();
    }

    // Méthode qui retourne tous les livres écrit par un auteur
    public List<Livre> findByAuteur(Auteur auteur) {
        TypedQuery<Livre> query = em.createQuery("SELECT l FROM Livre l JOIN l.auteurs a WHERE a=:auteur", Livre.class);
        query.setParameter("auteur", auteur);
        return query.getResultList();
    }

    // Méthode qui retourne tous les livres qui ont plusieurs auteurs
    public List<Livre> findMultiAuteur() {
        TypedQuery<Livre> query = em.createQuery("SELECT l FROM Livre l JOIN l.auteurs a GROUP BY l HAVING COUNT(a)>1", Livre.class);
        return query.getResultList();
    }

    // Méthode qui retourne le nombre de livres écrit (count) pour chaque auteur (concaténation du prénom et du nom => data)
    public List<StatLivre> getStatLivreByAuteur() {
        TypedQuery<StatLivre> query = em.createQuery("SELECT new StatLivre(Count(l),CONCAT(a.prenom, ' ', a.nom)) FROM Auteur a LEFT JOIN a.livres l GROUP BY a ORDER BY count(l) DESC", StatLivre.class);
        return query.getResultList();
    }

    // Méthode qui retourne le nombre de livres(count) pour chaque catégorie (nom de la catégorie => data) dans l'ordre décroissant
    public List<StatLivre> getStatLivreByCategorie() {
        TypedQuery<StatLivre> query = em.createQuery("SELECT new StatLivre(COUNT(l),l.categorie.nom) FROM Livre l GROUP BY l.categorie ORDER BY count(l) DESC", StatLivre.class);
        return query.getResultList();
    }

    // Méthode qui retourne l'année où est sortie le plus de livre
    public int getMaxLivreAnnee() {
        TypedQuery<Integer> query = em.createQuery(
                "SELECT l.anneeSortie FROM Livre l GROUP BY l.anneeSortie ORDER BY COUNT(l) DESC", Integer.class);
        return query.setMaxResults(1).getSingleResult();
    }

    // Sous-requête   
    // Méthode qui retourne les livres qui correspondent à l'année de sortie moyenne d'une catégorie
    public List<Livre> findByAvgAnnee(Categorie c) {
        TypedQuery<Livre> query = em.createQuery("SELECT li FROM Livre li WHERE li.anneeSortie = (SELECT TRUNCATE(AVG(l.anneeSortie)) FROM Livre l WHERE l.categorie=:cat)", Livre.class);
        query.setParameter("cat", c);
        return query.getResultList();
    }

    // Méthode qui retourne les livres qui sont sortie les même année que les livres de la catégorie passé en paramètre
    public List<Livre> findBySameAnnee(Categorie c) {
        TypedQuery<Livre> query = em.createQuery("SELECT li FROM Livre li WHERE li.anneeSortie= ANY (SELECT l.anneeSortie FROM Livre l WHERE l.categorie=:cat)", Livre.class);
        query.setParameter("cat", c);
        return query.getResultList();
    }

}