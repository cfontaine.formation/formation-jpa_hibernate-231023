package fr.dawan.exercicejpql.dao;

import java.util.List;

import fr.dawan.exercicejpql.entities.Auteur;
import fr.dawan.exercicejpql.entities.Categorie;
import fr.dawan.exercicejpql.entities.Nation;
import jakarta.persistence.EntityManager;
import jakarta.persistence.TypedQuery;

public class AuteurDao extends AbstractDao<Auteur> {

    private static final long serialVersionUID = 1L;

    public AuteurDao(EntityManager em) {
        super(em, Auteur.class);
    }

    // Requètes nommées
    
    // Méthode  qui retourne tous les auteurs vivant
    public List<Auteur> findAlive() {
        TypedQuery<Auteur> query = em.createNamedQuery("Auteur.alive", Auteur.class);
        return query.getResultList();
    }
    
    // Méthode qui retourne tous les auteurs qui n'ont pas écrit de livre
    public List<Auteur> findByNoBook() {
        TypedQuery<Auteur> query = em.createNamedQuery("Auteur.nobook", Auteur.class);
        return query.getResultList();
    }

    // Méthode qui retourne les auteurs en fonction de la nation passée en paramètre
    public List<Auteur> findByNation(Nation nation) {
        TypedQuery<Auteur> query = em.createNamedQuery("Auteur.byNation", Auteur.class);
        query.setParameter("nom", nation);
        return query.getResultList();
    }
    
    // setMaxResults
    // Méthode qui retourner le top 5 des auteurs qui ont écrit le plus de livre
    public List<Auteur> getTop5Auteur() {
        TypedQuery<Auteur> query = em.createQuery("SELECT a FROM Auteur a JOIN a.livres l GROUP BY a ORDER BY COUNT(l) DESC", Auteur.class);
        return query.setMaxResults(5).getResultList();
    }

    // Méthode qui retourne le top 10 des auteurs les plus anciens
    public List<Auteur> getTop10AgeAuteur() {
        TypedQuery<Auteur> query = em.createQuery("SELECT a FROM Auteur a ORDER BY a.dateNaissance", Auteur.class);
        return query.setMaxResults(10).getResultList();
    }

    // Méthode qui retourne tous les auteurs qui ont écrit un livre de la catégorie passé en paramètre
    public List<Auteur> findByCategorie(Categorie c) {
        TypedQuery<Auteur> query = em.createQuery("SELECT a FROM Auteur a JOIN a.livres l WHERE l.categorie=:categorie",Auteur.class);
        query.setParameter("categorie", c);
        return query.getResultList();
    }
}
