package fr.dawan.exercicejpql.dao;

import java.io.Serializable;
import java.util.List;

import fr.dawan.exercicejpql.entities.AbstractEntity;
import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityTransaction;
import jakarta.persistence.TypedQuery;

public abstract class AbstractDao<T extends AbstractEntity> implements Serializable {

    private static final long serialVersionUID = 1L;

    protected EntityManager em;

    private Class<T> classEntity;

    public AbstractDao(EntityManager em, Class<T> classEntity) {
        this.em = em;
        this.classEntity = classEntity;
    }

    public EntityManager getEm() {
        return em;
    }

    public void setEm(EntityManager em) {
        this.em = em;
    }

    public void saveOrUpdate(T elm) throws Exception {
        EntityTransaction tx = em.getTransaction();
        try {
            tx.begin();
            if (elm.getId() == 0) {
                em.persist(elm);
            } else {
                em.merge(elm);
            }
            tx.commit();
        } catch (Exception e) {
            tx.rollback();
            throw e;
        }
    }

    public void remove(T elm) throws Exception {
        EntityTransaction tx = em.getTransaction();

        try {
            tx.begin();
            if (elm.getId() != 0) {
                em.remove(elm);
            }
            tx.commit();
        } catch (Exception e) {
            tx.rollback();
            throw e;
        }
    }

    public T findbyId(long id) {
        return em.find(classEntity, id);
    }

    public List<T> findAll() {
        return em.createQuery("SELECT e FROM " + classEntity.getName() + " e", classEntity).getResultList();
    }

    // Ajouter à la classe AbstractDao une méthode retourne le nombre d'entité que
    // contient la table
    public long count() {
        TypedQuery<Long> tl = em.createQuery("SELECT count(e) FROM " + classEntity.getName() + " e", Long.class);
        return tl.getSingleResult();
    }
}
