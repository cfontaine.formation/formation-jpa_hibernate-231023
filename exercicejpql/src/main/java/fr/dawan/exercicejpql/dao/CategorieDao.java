package fr.dawan.exercicejpql.dao;

import fr.dawan.exercicejpql.entities.Categorie;
import jakarta.persistence.EntityManager;
import jakarta.persistence.TypedQuery;

public class CategorieDao extends AbstractDao<Categorie> {

    private static final long serialVersionUID = 1L;

    public CategorieDao(EntityManager em) {
        super(em, Categorie.class);
    }

    // Méthode qui retourne une catégorie en fonction du nom passé en paramètre
    public Categorie findByNom(String nom) {
        TypedQuery<Categorie> q = em.createQuery("SELECT c FROM Categorie c WHERE c.nom=:nom", Categorie.class);
        q.setParameter("nom", nom);
        return q.getSingleResult();
    }

}
