package fr.dawan.exercicejpql;

import java.util.List;

import fr.dawan.exercicejpql.dao.AuteurDao;
import fr.dawan.exercicejpql.dao.CategorieDao;
import fr.dawan.exercicejpql.dao.LivreDao;
import fr.dawan.exercicejpql.dao.NationDao;
import fr.dawan.exercicejpql.entities.Auteur;
import fr.dawan.exercicejpql.entities.Categorie;
import fr.dawan.exercicejpql.entities.Livre;
import fr.dawan.exercicejpql.entities.Nation;
import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.Persistence;

public class App {
    public static void main(String[] args) {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("exercicejpql");
        EntityManager em = emf.createEntityManager();

        LivreDao dao = new LivreDao(em);

        List<Livre> lst = dao.findAll();
        for (Livre l : lst) {
            System.out.println(l);
        }

        System.out.println(dao.count());

        // Tous les livres sortie en 1992
        dao.findByAnnee(1992).forEach(System.out::println);

        // Tous les livres sortie entre 1970 et 1980
        dao.findByIntervalAnnee(1970, 1980).forEach(System.out::println);

        // Tous les livres qui commencent par du
        dao.findStartWith("du").forEach(System.out::println);

        // Tous les livres qui sont sorti en 1992, 1954 et en 1982
        dao.findByAnnees(1992, 1954, 1982).forEach(System.out::println);

        // Le nombre de livre sortie en 1974
        System.out.println(dao.countByAnneeSortie(1974));

        // Tous les livres qui ont pour catégorie drame 
        CategorieDao daoCategorie = new CategorieDao(em);
        try {
            Categorie c = daoCategorie.findByNom("Drame");
            dao.findByCategorie(c).forEach(System.out::println);
        } catch (Exception e) {
            System.err.println("La categorie n'existe pas");
        }

        // Tous les livres de l'auteur qui a pour id=1
        Auteur a = em.find(Auteur.class, 1L);
        dao.findByAuteur(a).forEach(System.out::println);

        // Tous les livres qui ont plusieurs auteurs
        dao.findMultiAuteur().forEach(System.out::println);

        // Le nombre de livres écrit pour chaque auteur (prénom et nom)
        dao.getStatLivreByAuteur().forEach(System.out::println);

        // Le nombre de livres pour chaque nom de catégorie dans l'ordre décroissant
        dao.getStatLivreByCategorie().forEach(System.out::println);

        // L'année où est sortie le plus de livre
        System.out.println(dao.getMaxLivreAnnee());
    
        Categorie ca = daoCategorie.findbyId(2L);
        
        // Les livres qui correspondent à l'année de sortie moyenne de la catégorie id=2
        dao.findByAvgAnnee(ca).forEach(System.out::println);
        
        // Les livres qui sont sortie les même années que les livres de la catégorie id=2
        dao.findBySameAnnee(ca).forEach(System.out::println);

        AuteurDao auteurDao = new AuteurDao(em);
        
        // Tous les auteurs vivant
        auteurDao.findAlive().forEach(System.out::println);

        // Tous les auteurs qui n'ont pas écrit de livre
        auteurDao.findByNoBook().forEach(System.out::println);

        NationDao nationDao = new NationDao(em);
        Nation nation = nationDao.findbyId(1L);
        
        // Les auteurs de la nation id=1
        auteurDao.findByNation(nation).forEach(System.out::println);

        // Le top 5 des auteurs qui ont écrit le plus de livre
        auteurDao.getTop5Auteur().forEach(System.out::println);
        
        // Le top 10 des auteurs les plus anciens
        auteurDao.getTop10AgeAuteur().forEach(System.out::println);

        // Tous les auteurs qui ont écrit un livre de la catégorie id=2
        auteurDao.findByCategorie(ca).forEach(System.out::println);

        em.close();
        emf.close();
    }
}
