package fr.dawan.formationjpa;

import java.util.List;

import fr.dawan.formationjpa.dao.PersonneDao;
import fr.dawan.formationjpa.entities.heritage.Personne;
import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.Persistence;

public class Main09Dao {

    public static void main(String[] args) {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("formationjpa");
        EntityManager em = emf.createEntityManager();

        PersonneDao dao = new PersonneDao(em);
        Personne per1 = new Personne("John", "Doe");
        dao.saveOrUpdate(per1);

        Personne per2 = new Personne("Yves", "Roulo");
        dao.saveOrUpdate(per2);

        List<Personne> lst = dao.findAll();
        for (Personne e : lst) {
            System.out.println(e);
        }

        long idPer2 = per2.getId();
        Personne p = dao.findById(idPer2);
        System.out.println(p);

        per2.setNom("Roulaut");
        dao.saveOrUpdate(per2);

        p = dao.findById(idPer2);
        System.out.println(p);

        // dao.remove(per2);
        dao.remove(idPer2);
        em.close();
        emf.close();
    }
}
