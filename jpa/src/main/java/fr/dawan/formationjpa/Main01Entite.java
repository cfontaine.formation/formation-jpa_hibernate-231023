package fr.dawan.formationjpa;

import java.time.LocalDate;

import fr.dawan.formationjpa.entities.Employe;
import fr.dawan.formationjpa.entities.Entreprise;
import fr.dawan.formationjpa.enums.TypeContrat;
import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.EntityTransaction;
import jakarta.persistence.Persistence;

public class Main01Entite {

    public static void main(String[] args) {
        // Création Entity Manager factory
        // createEntityManagerFactory prend en paramètre le nom de la persistence-unit définie dans persitence.xml
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("formationjpa");

        // Création Entity Manager
        EntityManager em = emf.createEntityManager();

        // Création de la transaction
        EntityTransaction tx = em.getTransaction();

        Employe emp1 = new Employe("John", "Doe", 2000.0, TypeContrat.CDI, LocalDate.of(1995, 10, 20));
        emp1.getTelephones().add("03.20.00.00.00");
        emp1.getTelephones().add("07.00.00.00.00");
        Employe emp2 = new Employe("Jane", "Doe", 2200.0, TypeContrat.CDI, LocalDate.of(1999, 1, 2));
        Entreprise e1 = new Entreprise("acme");

        try {
            tx.begin(); // -> démarrer la transaction
            em.persist(emp1); // Peristence de l'objet emp1 dans la bdd
            em.persist(emp2);
            em.persist(e1);

            // emp1 est géré par le entité manager -> quand on modifie l'état de emp1, elle est persitée dans la bdd
            emp1.setSalaire(2010.00);

            // Suppression de l'objet emp2 dans la bdd
            em.remove(emp2);

            tx.commit(); // transaction -> valider
        } catch (Exception e) {
            e.printStackTrace();
            tx.rollback(); // transaction -> annuler
        }

        em.close();     // fermeture de l'entity manager
        emf.close();    // fermeture de l'entity manager factory
    }
}
