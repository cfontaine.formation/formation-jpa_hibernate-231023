package fr.dawan.formationjpa.enums;

public enum TypeContrat {
    CDI, CDD, INTERIM, STAGE, ALTERNANT
}
