package fr.dawan.formationjpa;

import java.time.LocalDate;

import fr.dawan.formationjpa.entities.relation.Article;
import fr.dawan.formationjpa.entities.relation.Marque;
import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.EntityTransaction;
import jakarta.persistence.Persistence;

public class Main08Cascade {

    public static void main(String[] args) {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("formationjpa");
        EntityManager em = emf.createEntityManager();
        EntityTransaction tx = em.getTransaction();
        
        Marque ma=new Marque("Marque A",LocalDate.of(1998, 7, 13));
        Marque mb=new Marque("Marque B",LocalDate.of(1998, 7, 13));
        Article a1 = new Article(100.0, "Disque SSD 2Go");
        Article a2 = new Article(140.0, "Carte mère AMD");
        Article a3 = new Article(640.0, "Carte graphique AMD");
        ma.getArticles().add(a1);
        ma.getArticles().add(a2);
        a1.setMarque(ma);
        a2.setMarque(ma);
        
        try {
            tx.begin();
            em.persist(ma);
            
            a1.setMarque(null);
            ma.getArticles().remove(a1);
            tx.commit();
        } catch (Exception e) {
            tx.rollback();
            e.printStackTrace();
        }
        em.close();
        emf.close();

    }

}
