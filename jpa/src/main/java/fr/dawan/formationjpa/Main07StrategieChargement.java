package fr.dawan.formationjpa;

import java.time.LocalDate;

import fr.dawan.formationjpa.entities.relation.Article;
import fr.dawan.formationjpa.entities.relation.Marque;
import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.EntityTransaction;
import jakarta.persistence.Persistence;

public class Main07StrategieChargement {

    public static void main(String[] args) {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("formationjpa");
        EntityManager em = emf.createEntityManager();
        EntityTransaction tx = em.getTransaction();

        Article a1 = new Article(100.0, "Disque SSD 2Go");
        Article a2 = new Article(140.0, "Carte mère AMD");
        Marque ma = new Marque("Marque A", LocalDate.of(1986, 10, 10));
        a1.setMarque(ma);
        ma.getArticles().add(a1);
        a2.setMarque(ma);
        ma.getArticles().add(a2);

        try {
            tx.begin();
            em.persist(a1);
            em.persist(a2);
            em.persist(ma);
            tx.commit();
        } catch (Exception e) {
            tx.rollback();
            e.printStackTrace();
        }
        em.clear(); // retirer les objets de l'entité manager

        // Chargement immédiat :
        Article al1 = em.find(Article.class, 1L);
        System.out.println("--------------------------------------");
        System.out.println(al1);
        // @ManyToOne -> EAGER : la variable d'instance marque est chargée au moment du find
        System.out.println(al1.getMarque()); 

        // Chargement tardif :
        Marque ml1 = em.find(Marque.class, 1L);
        System.out.println("--------------------------------------");
        System.out.println(ml1);
        // @OneToMany -> LAZY : les éléments de la collection articles sont chargés au moment où l'on y accéde
        System.out.println(ml1.getArticles().get(0)); // 1
        
        em.close(); // On ferme l'EntityManger 

        // Si on commente //1 et on décommente //2
        // On accède à un élément de articles, mais l'entity manager est fermé
        // Il n'y a plus de connection à la base de donnée ->
        // LazyInitializationException
        
        // System.out.println(ml1.getArticles().get(0)); // 2
        
        emf.close();

    }

}
