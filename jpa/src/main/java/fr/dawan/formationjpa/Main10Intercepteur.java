package fr.dawan.formationjpa;

import java.util.List;

import fr.dawan.formationjpa.entities.interceptors.Facture;
import fr.dawan.formationjpa.entities.interceptors.Utilisateur;
import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.EntityTransaction;
import jakarta.persistence.Persistence;

public class Main10Intercepteur {

    public static void main(String[] args) {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("formationjpa");
        EntityManager em = emf.createEntityManager();
        EntityTransaction tx = em.getTransaction();
        
        Facture fact1=new Facture(2100.0,5.5);
        fact1.calculMontantTtc();
        Utilisateur u=new Utilisateur("John","Doe","jd@dawan.com","azerty");
        
        System.out.println(fact1);
        try {
            tx.begin();
            
            // exemple 1: facture
            em.persist(fact1);
            //exemple 2: auditing
            em.persist(u);
            u.setPassword("123456");
            tx.commit();
            em.clear();
        } catch (Exception e) {
            tx.rollback();
            e.printStackTrace();
        }
        
        // suite exemple 1: facture 
        List<Facture> lstFact=em.createQuery("SELECT f FROM Facture f",Facture.class).getResultList();
        lstFact.forEach(System.out::println);
        em.close();
        emf.close();

    }

}
