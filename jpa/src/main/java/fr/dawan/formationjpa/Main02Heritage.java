package fr.dawan.formationjpa;

import fr.dawan.formationjpa.entities.heritage.CompteBancaire;
import fr.dawan.formationjpa.entities.heritage.CompteEpargne;
import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.EntityTransaction;
import jakarta.persistence.Persistence;

public class Main02Heritage {

    public static void main(String[] args) {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("formationjpa");
        EntityManager em = emf.createEntityManager();
        EntityTransaction tx = em.getTransaction();

        CompteBancaire cb1 = new CompteBancaire(200, "fr-3562647", "John Doe");
        CompteBancaire cb2 = new CompteBancaire(500, "fr-356822", "Jane Doe");

        CompteEpargne ce1 = new CompteEpargne(50, "fr-987533", "John Doe", 6.0);
        CompteEpargne ce2 = new CompteEpargne(500, "fr-487533", "Alan Smithee", 3.0);

        try {
            tx.begin();
            em.persist(cb1);
            em.persist(cb2);
            em.persist(ce1);
            em.persist(ce2);
            tx.commit();
        } catch (Exception e) {
            tx.rollback();
            e.printStackTrace();
        }
        em.close();
        emf.close();
    }
}
