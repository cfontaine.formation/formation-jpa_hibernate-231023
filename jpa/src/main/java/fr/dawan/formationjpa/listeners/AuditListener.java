package fr.dawan.formationjpa.listeners;

import java.time.LocalDateTime;

import fr.dawan.formationjpa.entities.interceptors.AbstractAuditable;
import jakarta.persistence.PrePersist;
import jakarta.persistence.PreUpdate;

public class AuditListener {

    @PrePersist
    public void onPersist(AbstractAuditable a) {
        System.out.println("@Prepersit");
        a.setCreated(LocalDateTime.now());
    }

    @PreUpdate
    public void onUpdate(AbstractAuditable a) {
        System.out.println("@PreUpdate");
        a.setModified(LocalDateTime.now());
    }

}
