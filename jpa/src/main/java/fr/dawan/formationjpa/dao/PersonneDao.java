package fr.dawan.formationjpa.dao;

import fr.dawan.formationjpa.entities.heritage.Personne;
import jakarta.persistence.EntityManager;

public class PersonneDao extends AbstractDao<Personne> {

    public PersonneDao(EntityManager em) {
        super(em, Personne.class);
    }
}
