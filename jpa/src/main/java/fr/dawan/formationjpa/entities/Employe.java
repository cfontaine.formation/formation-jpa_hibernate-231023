package fr.dawan.formationjpa.entities;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import fr.dawan.formationjpa.enums.TypeContrat;
import jakarta.persistence.AttributeOverride;
import jakarta.persistence.AttributeOverrides;
import jakarta.persistence.CollectionTable;
import jakarta.persistence.Column;
import jakarta.persistence.ElementCollection;
import jakarta.persistence.Embedded;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.Lob;
import jakarta.persistence.Table;
import jakarta.persistence.Transient;
import jakarta.persistence.Version;

//une entité doit :
//- être annoté avec @Entity
//- avoir un attribut qui représente la clé primaire annoté avec @Id
//- implémenté l'interface Serializable
//- avoir obligatoirement un contructeur par défaut

@Entity
@Table(name = "employes") // L'annotation @Table permet de modifier le nom de la table, sinon elle prend le nom de la classe
public class Employe implements Serializable {

    private static final long serialVersionUID = 1L;

    // @Id -> Clé primaire simple
    @Id

    // Génération automatique de la clé primaire 
    // 1 -> AUTO (par défaut): Hibernate choisit une stratégie en fonction du SGBD 
    // @GeneratedValue
    
    // 2 -> IDENTITY: c'est la base de donnée qui va générer la clé primaire
    //      Mysql, MariaDB -> AUTO_INCREMENT
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    
    // 3 -> TABLE: Hibernate crée une table (ici table_gen) qui stocke les noms et les valeurs des séquences
    // à utiliser avec l’annotation @TableGenerator
    // @TableGenerator(name="table_gen")
    // @GeneratedValue(strategy = GenerationType.TABLE,generator ="table_gen")
    
    // 4 -> SEQUENCE: La génération se fait par une séquence définie par le système de gestion de bdd
    // utiliser avec l’annotation @SequenceGenerator
    // @SequenceGenerator(name="emp_seq")
    // @GeneratedValue(strategy = GenerationType.SEQUENCE,generator = "emp_seq")
    private long id;
    
    // Gestion de la concurrence Optimistic (Versioned) => @Version
    @Version
    private int version;

    // L'attribut @Column permet pour définir plus précisément la colonne
    @Column(length = 50) // l'attribut length permet modifier de la longueur d'une chaine de caractère ou d'un @lob
    private String prenom; // Sinon par défaut 255 caractères ou octets

    @Column(length = 50, nullable = false)   // l'attribut nullable permet de définir si le champ peut être null (optionelle) par défaut true
    private String nom;

    private double salaire;

    // Une énumération peut être stocké sous forme numérique EnumType.ORDINAL (par défaut)
    // ou sous forme de chaine de caractères EnumType.STRING
    @Enumerated(EnumType.STRING)
    // @Column(length=15)
    private TypeContrat contrat;

    // LocalDate, LocalTime, LocalDateTime sont supportés par hibernate depuis la version 5
    @Column(name = "date_naissance", nullable = false)
    private LocalDate dateNaissance;

    // @Lob => pour stocker des données binaires dans la bdd (image, ...) BLOB ou un long texte CLOB
  //  @Basic(fetch = FetchType.LAZY)
    @Lob 
    @Column(length = 70000) // l'attribut length permet de définir la taille du blob => ici un MediumBlob max 16 mo
                           // pour MYSQL : par défaut un Tinyblob -> 255, Blob-> 65535, MediumBlob -> 16  mo, LongBlob -> 4,29 Go
    private byte[] photoIdentite; // BLOB -> tableau de byte , CLOB -> tableau de caractère

    
    // @ Embedded => pour utiliser une classe intégrable
    @Embedded
    private Adresse adressePerso;

    // Pour utiliser plusieurs fois la même classe imbriqué dans la même entitée
    // On aura plusieurs fois le même nom de colonne dans la table => erreur
    // On pourra renommé les colonnes avec des annotations @AttributeOverride placé dans une annotation @AttributeOverrides
    @Embedded
    @AttributeOverrides({ @AttributeOverride(name = "rue", column = @Column(name = "rue_pro")),
            @AttributeOverride(name = "ville", column = @Column(name = "ville_pro")),
            @AttributeOverride(name = "codePostal", column = @Column(name = "code_postal_pro", length = 15)) })
    private Adresse adressePro;

    // Les attributs @Transient ne sont transient ne sont pas persister
    // sinon tous les autres sont par défaut persistant
    @Transient
    private int nePasPersiter;
    // ou private transient int nePasPersiter;
    
    // Mapping d'une collection simple (String, LocalDate, Integer,Double ...)
    @ElementCollection // @ElementCollection -> par défaut, génére une table NomClasse_nomVariable
 // @CollectionTable -> permet de personaliser de la table name: nom de la table, joinColuns -> nom de la colonne de jointure 
    @CollectionTable(name="telephones",joinColumns = @JoinColumn(name="employes_id"))
    @Column(name="telephone2",length = 15)
    private List<String> telephones=new ArrayList<>();

    // Constructeur par défaut -> obligatoire avec Hibernate
    public Employe() {
    }

    public Employe(String prenom, String nom, double salaire, TypeContrat contrat, LocalDate dateNaissance) {
        this.prenom = prenom;
        this.nom = nom;
        this.salaire = salaire;
        this.contrat = contrat;
        this.dateNaissance = dateNaissance;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public double getSalaire() {
        return salaire;
    }

    public void setSalaire(double salaire) {
        this.salaire = salaire;
    }

    public TypeContrat getContrat() {
        return contrat;
    }

    public void setContrat(TypeContrat contrat) {
        this.contrat = contrat;
    }

    public LocalDate getDateNaissance() {
        return dateNaissance;
    }

    public void setDateNaissance(LocalDate dateNaissance) {
        this.dateNaissance = dateNaissance;
    }

    public byte[] getPhotoIdentite() {
        return photoIdentite;
    }

    public void setPhotoIdentite(byte[] photoIdentite) {
        this.photoIdentite = photoIdentite;
    }

    public Adresse getAdressePerso() {
        return adressePerso;
    }

    public void setAdressePerso(Adresse adressePerso) {
        this.adressePerso = adressePerso;
    }

    public Adresse getAdressePro() {
        return adressePro;
    }

    public void setAdressePro(Adresse adressePro) {
        this.adressePro = adressePro;
    }

    public int getNePasPersiter() {
        return nePasPersiter;
    }

    public void setNePasPersiter(int nePasPersiter) {
        this.nePasPersiter = nePasPersiter;
    }

    public List<String> getTelephones() {
        return telephones;
    }

    public void setTelephones(List<String> telephones) {
        this.telephones = telephones;
    }

    @Override
    public String toString() {
        return "Employe [id=" + id + ", prenom=" + prenom + ", nom=" + nom + ", salaire=" + salaire + ", contrat="
                + contrat + ", dateNaissance=" + dateNaissance + ", adressePerso=" + adressePerso + ", adressePro="
                + adressePro + ", nePasPersiter=" + nePasPersiter + "]";
    }
}
