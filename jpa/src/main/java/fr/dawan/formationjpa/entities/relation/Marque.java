package fr.dawan.formationjpa.entities.relation;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import jakarta.persistence.Version;

@Entity
@Table(name = "marques")
public class Marque implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Version
    private int version;

    @Column(length = 50, nullable = false)
    private String nom;

    // Relation 1,n bidirectionnelle -> @OneToMany 
    // C'est la relation retour Marque -> Article
    // pour la réaliser, on utilise un collection qui contient les articles
    // on utilise l'annotation @OneToMany avec l'attribut mappedBy qui a pour valeur
    // le nom de variable d'instance de l'autre coté de la relation -> ici marque
    // dans ce cas, on peut connaitre le marque à partir de l'article et les articles
    // depuis la marque
    
    // @OneToMany(mappedBy = "marque",cascade = {CascadeType.PERSIST,CascadeType.REMOVE})// ,fetch=FetchType.EAGER
    @OneToMany(mappedBy = "marque", cascade = CascadeType.ALL, orphanRemoval = true) // ,fetch=FetchType.EAGER
    private List<Article> articles = new ArrayList<>();

    private LocalDate dateCreation;

    public Marque() {
    }

    public Marque(String nom, LocalDate dateCreation) {
        this.nom = nom;
        this.dateCreation = dateCreation;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public LocalDate getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(LocalDate dateCreation) {
        this.dateCreation = dateCreation;
    }

    public List<Article> getArticles() {
        return articles;
    }

    public void setArticles(List<Article> articles) {
        this.articles = articles;
    }

    @Override
    public String toString() {
        return "Marque [id=" + id + ", nom=" + nom + ", dateCreation=" + dateCreation + "]";
    }
}
