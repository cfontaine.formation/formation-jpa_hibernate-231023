package fr.dawan.formationjpa.entities.heritage;

import fr.dawan.formationjpa.entities.AbstractEntity;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;

@Entity
@Table(name = "personnes")
public class Personne extends AbstractEntity {

    private static final long serialVersionUID = 1L;

    private String prenom;

    private String nom;

    public Personne() {
    }

    public Personne(String prenom, String nom) {
        this.prenom = prenom;
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    @Override
    public String toString() {
        return "Personne [prenom=" + prenom + ", nom=" + nom + super.toString() + "]";
    }
}
