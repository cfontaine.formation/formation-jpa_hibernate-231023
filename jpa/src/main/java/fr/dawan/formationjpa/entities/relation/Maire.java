package fr.dawan.formationjpa.entities.relation;

import java.io.Serializable;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToOne;
import jakarta.persistence.Table;
import jakarta.persistence.Version;

@Entity
@Table(name = "maires")
public class Maire implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(length = 50, nullable = false)
    private String nom;

    @Version
    private int version;

    // Relation @OneToOne bidirectionnelle
    // C'est la relation retour Maire -> Ville
    // pour la réaliser, on place sur la variable d'instance de type Ville
    // on utilise l'annotation @OneToOne avec l'attribut mappedBy qui a pour valeur
    // le nom de variable d'instance de l'autre coté de la relation -> ici maire
    // dans ce cas, on peut connaitre le maire à partir de la ville et la ville
    // depuis le maire

    @OneToOne(mappedBy = "maire")
    private Ville ville;

    // Pour Relation @OneToOne unidirectionnelle
    // On n'a pas de type Ville dans Maire, pour réaliser la relation retour
    // dans ce cas, on peut connaitre le maire à partir de la ville, mais pas la
    // ville depuis le maire

    public Maire() {
    }

    public Maire(String nom) {
        this.nom = nom;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public Ville getVille() {
        return ville;
    }

    public void setVille(Ville ville) {
        this.ville = ville;
        ville.setMaire(this);
    }

    @Override
    public String toString() {
        return "Maire [id=" + id + ", nom=" + nom + "]";
    }
}
