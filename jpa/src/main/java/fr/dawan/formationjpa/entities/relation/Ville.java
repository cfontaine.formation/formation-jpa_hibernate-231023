package fr.dawan.formationjpa.entities.relation;

import java.io.Serializable;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToOne;
import jakarta.persistence.Table;
import jakarta.persistence.Version;

@Entity
@Table(name="villes")
public class Ville implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    
    @Version
    private int version;
    
    @Column(length = 50,nullable=false)
    private String nom;
    
    // @OneToOne => Relation 1,1
    // ici, Un maire n'est que d'une seul ville et une ville à seul maire

    // Relation @OneToOne Unidirectionnel
    // on a uniquement une relation Ville -> Maire
    // et pas de relation Maire -> Ville , on n'a pas accés au Ville depuis le Maire

    
    @OneToOne
    // @JoinColumn(name="id_maire") -> @JoinColumn permet de modifier le nom par défaut de la colonne 
    private Maire maire;
    
    // Relation @OneToOne Bidirectionnel => voir dans la classe Maire
    // on a une relation Ville -> Maire et Maire -> Ville

    public Ville() {
    }

    public Ville(String nom) {
        this.nom = nom;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public Maire getMaire() {
        return maire;
    }

    public void setMaire(Maire maire) {
        this.maire = maire;
        maire.setVille(this);
    }

    @Override
    public String toString() {
        return "Ville [id=" + id + ", nom=" + nom + "]";
    }
}
