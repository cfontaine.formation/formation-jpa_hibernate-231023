package fr.dawan.formationjpa.entities.relation;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import jakarta.persistence.Version;

@Entity
@Table(name = "articles")
public class Article implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Version
    private int version;

    @Column(nullable = false)
    private double prix;

    @Column(length = 100, nullable = false)
    private String description;

    // @ManyToOne -> Relations 1,n
    // Un article a une marqueet une marque peut avoir plusieurs articles
    
    // Relation @ManyToOne Unidirectionnel
    // on a uniquement une relation Article -> Marque
    // et pas de relation Marque -> Article , on n'a pas accés aux articles depuis le Marque
    @ManyToOne
    @JoinColumn(name = "id_marque")
    private Marque marque;

    @ManyToMany(mappedBy = "articles")
    private List<Fournisseur> fournisseurs = new ArrayList<>();

    public Article() {
    }

    public Article(double prix, String description) {
        this.prix = prix;
        this.description = description;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public double getPrix() {
        return prix;
    }

    public void setPrix(double prix) {
        this.prix = prix;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Marque getMarque() {
        return marque;
    }

    public void setMarque(Marque marque) {
        this.marque = marque;
    }

    public void addMarque(Marque marque) {
        this.marque = marque;
        marque.getArticles().add(this);
    }

    public List<Fournisseur> getFournisseurs() {
        return fournisseurs;
    }

    public void setFournisseurs(List<Fournisseur> fournisseurs) {
        this.fournisseurs = fournisseurs;
    }

    @Override
    public String toString() {
        return "Article [id=" + id + ", version=" + version + ", prix=" + prix + ", description=" + description + "]";
    }

}
