package fr.dawan.formationjpa.entities;

import jakarta.persistence.Column;
import jakarta.persistence.Embeddable;

// Une classe intégrable va stocker ses données dans la
// table de l’entité mère ce qui va créer des colonnes supplémentaires

// On annote une classe intégrable avec @Embeddable
@Embeddable
public class Adresse {

    private String rue;

    private String ville;

    @Column(length = 15, name = "code_postal")
    private String codePostal;

    public Adresse() {
    }

    public Adresse(String rue, String ville, String codePostal) {
        this.rue = rue;
        this.ville = ville;
        this.codePostal = codePostal;
    }

    public String getRue() {
        return rue;
    }

    public void setRue(String rue) {
        this.rue = rue;
    }

    public String getVille() {
        return ville;
    }

    public void setVille(String ville) {
        this.ville = ville;
    }

    public String getCodePostal() {
        return codePostal;
    }

    public void setCodePostal(String codePostal) {
        this.codePostal = codePostal;
    }

    @Override
    public String toString() {
        return "Adresse [rue=" + rue + ", ville=" + ville + ", codePostal=" + codePostal + "]";
    }
}
