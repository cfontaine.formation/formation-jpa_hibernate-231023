package fr.dawan.formationjpa.entities.interceptors;

import java.io.Serializable;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.PostLoad;
import jakarta.persistence.PrePersist;
import jakarta.persistence.Table;
import jakarta.persistence.Transient;
import jakarta.persistence.Version;

@Entity
@Table(name="factures")
public class Facture implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    
    @Version
    private int version;
    
    private double montantHt;
    
    private double tva;
    
    @Transient
    private double montantTtc;

    public Facture() {
    }

    public Facture(double montantHt, double tva) {
        this.montantHt = montantHt;
        this.tva = tva;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public double getMontantHt() {
        return montantHt;
    }

    public void setMontantHt(double montantHt) {
        this.montantHt = montantHt;
    }

    public double getTva() {
        return tva;
    }

    public void setTva(double tva) {
        this.tva = tva;
    }

    public double getMontantTtc() {
        return montantTtc;
    }

    public void setMontantTtc(double montantTtc) {
        this.montantTtc = montantTtc;
    }
    
    @PrePersist
    public void onPrepersist() {
        System.out.println("Pre persist");
    }
    
    @PostLoad
    public void calculMontantTtc() {
        System.out.println("Post Load");
        montantTtc=montantHt * (tva/100);
    }

    @Override
    public String toString() {
        return "Facture [id=" + id + ", montantHt=" + montantHt + ", tva=" + tva + ", montantTtc=" + montantTtc + "]";
    }
    
    
}
