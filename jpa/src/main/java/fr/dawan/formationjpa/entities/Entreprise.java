package fr.dawan.formationjpa.entities;

import java.io.Serializable;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

@Entity
@Table(name = "entreprises")
public class Entreprise implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    // @TableGenerator(name="table_gen")
    // @GeneratedValue(strategy = GenerationType.TABLE,generator = "table_gen")
    // @SequenceGenerator(name="ent_seq")
    // @GeneratedValue(strategy = GenerationType.SEQUENCE,generator = "ent_seq")
    private long id;

    private String nom;

    public Entreprise() {
    }

    public Entreprise(String nom) {
        this.nom = nom;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    @Override
    public String toString() {
        return "Entreprise [id=" + id + ", nom=" + nom + "]";
    }
}
