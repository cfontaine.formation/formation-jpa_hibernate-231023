package fr.dawan.formationjpa;

import java.time.LocalDate;
import java.util.Scanner;

import fr.dawan.formationjpa.entities.Adresse;
import fr.dawan.formationjpa.entities.Employe;
import fr.dawan.formationjpa.enums.TypeContrat;
import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.EntityTransaction;
import jakarta.persistence.Persistence;

public class Main06CycleDeVie {

    public static void main(String[] args) {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("formationjpa");
        EntityManager em = emf.createEntityManager();
        EntityTransaction tx = em.getTransaction();
        
        Employe emp1= new Employe("John","Doe",2000.0,TypeContrat.CDI,LocalDate.of(1989, 7, 11));
        Adresse adrPerso=new Adresse("1, rue Esquermoise","Lille","59800");
        Adresse adrPro=new Adresse("46, rue des cannoniers","Lille","59800");
        emp1.setAdressePerso(adrPerso);
        emp1.setAdressePro(adrPro);
        
        System.out.println(emp1);
        try {
            tx.begin();
            // Persister
           // em.persist(emp1);
            // detached
            //em.detach(emp1);
            //emp1.setSalaire(2002);
           // em.merge(emp1);
           // System.out.println(emp1);
            tx.commit();
        } catch (Exception e) {
            tx.rollback();
            e.printStackTrace();
        }
        em.clear();
        emp1=null;
        emp1=em.find(Employe.class, 3L);
        System.out.println(emp1);
        
        Scanner sc=new Scanner(System.in);
        int i=sc.nextInt();
        System.out.println(emp1);
        em.refresh(emp1);
        System.out.println(emp1);
        em.close();

        
        EntityManager em2 = emf.createEntityManager();
        EntityTransaction tx2 = em2.getTransaction();
        
        try {
            tx2.begin();
            emp1.setSalaire(2020.0);
            em2.merge(emp1);
            System.out.println(emp1);
            tx2.commit();
        } catch (Exception e) {
            tx2.rollback();
            e.printStackTrace();
        }
        em2.close();
        emf.close();

    }

}
