package fr.dawan.formationjpa;

import java.time.LocalDate;

import fr.dawan.formationjpa.entities.relation.Article;
import fr.dawan.formationjpa.entities.relation.Marque;
import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.EntityTransaction;
import jakarta.persistence.Persistence;

public class Main04ManyToOne {

    public static void main(String[] args) {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("formationjpa");
        EntityManager em = emf.createEntityManager();
        EntityTransaction tx = em.getTransaction();

        Article a1 = new Article(30.0, "clavier");
        Article a2 = new Article(50.0, "Souris Gaming");
        Article a3 = new Article(550.0, "TV 4K");

        Marque mA = new Marque("Marque A", LocalDate.of(1985, 10, 10));
        Marque mB = new Marque("Marque B", LocalDate.of(1998, 1, 1));

        a1.setMarque(mA);
        mA.getArticles().add(a1);

        a2.setMarque(mA);
        mA.getArticles().add(a2);

        a3.setMarque(mB);
        mB.getArticles().add(a3);
        try {
            tx.begin();
            em.persist(a1);
            em.persist(a2);
            em.persist(a3);
            em.persist(mA);
            em.persist(mB);
            tx.commit();
        } catch (Exception e) {
            tx.rollback();
            e.printStackTrace();
        }
        em.close();
        emf.close();
    }
}
