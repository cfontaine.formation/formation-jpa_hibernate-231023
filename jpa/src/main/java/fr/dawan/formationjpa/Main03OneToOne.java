package fr.dawan.formationjpa;

import fr.dawan.formationjpa.entities.relation.Maire;
import fr.dawan.formationjpa.entities.relation.Ville;
import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.EntityTransaction;
import jakarta.persistence.Persistence;

public class Main03OneToOne {

    public static void main(String[] args) {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("formationjpa");
        EntityManager em = emf.createEntityManager();
        EntityTransaction tx = em.getTransaction();

        Ville lille = new Ville("lille");
        Maire martine = new Maire("martine");
        lille.setMaire(martine);

        try {
            tx.begin();
            em.persist(martine);
            em.persist(lille);
            tx.commit();
        } catch (Exception e) {
            tx.rollback();
            e.printStackTrace();
        }
        em.close();
        emf.close();
    }
}
